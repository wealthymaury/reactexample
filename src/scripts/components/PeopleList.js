import React from 'react';
import Person from './Person'

export default class PeopleList extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		var personNodes = this.props.data.map(function(person) {
			return (
				<Person person={person} key={person.name} />
			);
		});

		return (
			<ul className="block-list comment-block">
				{personNodes}
			</ul>
		);
	}
}
