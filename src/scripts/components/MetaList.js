import React from 'react';
import BaseComponent from './BaseComponent';

export default class MetaList extends BaseComponent {

	constructor(props) {
		super(props);

		//this._handleAction = this._handleAction.bind(this);
		this._bind('_handleAction');

		this.state = { 
			like: false, 
			block: false, 
			ignore: false 
		};
	}

	render() {
		var like = this.state.like ? 'unlike' : 'like';
		var block = this.state.block ? 'unblock' : 'block';
		var ignore = this.state.ignore ? 'follow' : 'ignore';

		return (
			<ul className="list-meta">
				<li><a href="#" className="button" onClick={this._handleAction.bind(this, 'like')}>{like}</a></li>
				<li><a href="#" className="button" onClick={this._handleAction.bind(this, 'block')}>{block}</a></li>
				<li><a href="#" className="button" onClick={this._handleAction.bind(this, 'ignore')}>{ignore}</a></li>
			</ul>
		);
	}

	_handleAction(type, e) {
		var state = this.state;
		state[type] = !state[type];
		
		e.preventDefault();

		this.setState(state);
		this.props.onActionStateChange(state);
	}
}

MetaList.types = {
	like: 'like',
	unlike: 'like',
	block: 'block',
	unblock: 'block',
	follow: 'ignore',
	ignore: 'ignore'
};
