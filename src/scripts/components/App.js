import $ from 'jquery';
import React from 'react';
import PeopleList from './PeopleList';

export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = { 
			data: [
				{ name: "Antonin", description: "Web developer" },
				{ name: "Juno", description: "Web designer" },
				{ name: "Alix", description: "Web designer" }
			]
		};
	}

	loadPeople() {
		 $.ajax({
			url: '/api/users',
			dataType: 'json',
			success: function(data) {
				//changing state
				this.setState({data: data})
			}.bind(this),
			error: function() {

			}
		});
	}

	componentDidMount() {
		this.loadPeople();
		//setInterval(this.loadPeople, 2000);
	}

	render() {
		return (
			<PeopleList data={this.state.data} />
		);
	}
}