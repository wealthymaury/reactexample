import React from 'react';
import MetaList from './MetaList'
import BaseComponent from './BaseComponent';

export default class Person extends BaseComponent {
	constructor(props) {
		super(props);

		this._bind('handleActionState');

		this.state = {
			actions: {
				like: false,
				block: false,
				ignore: false
			}
		};
	}

	render() {
		var classes = this.buildClassNames();

		return (
			<li className={classes}>
				<h4 className="secondary-dark">
					{this.props.person.name}
				</h4>
				<p>
					{this.props.person.description}
				</p>
				<MetaList onActionStateChange={this.handleActionState} />
			</li>
		);
	}

	handleActionState(newState) {
		this.setState({ actions: newState });
	}

	buildClassNames() {
	   var actions = this.state.actions;
	   var classes = [];

	   for(var action in actions) {
			if(actions[action]) {
				classes.push('person-' + action);
			}
	   }

	   return classes.join(' ');
	}
}
