/* cacheo de plugins de node_modules */

var gulp = require('gulp');
var webserver = require('gulp-webserver');
var stylus = require('gulp-stylus');
var nib = require('nib');
var minifyCSS = require('gulp-minify-css');
var browserify = require('browserify');
var reactPreset = require('babel-preset-react');
var es2015Preset = require('babel-preset-es2015');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');

/* Configuracion de directorios */

var config = {
	styles: {
		main: './src/stylus/master.styl', // archivo de entrada
		watch: './src/stylus/**/*.styl',  // para ver todos los .styl de la carpeta styles
		output: './build/css'
	},
	html: {
		main: './src/**/*.html',
		watch: './src/*.html',
		output: './build'
	},
	scripts: {
		main: './src/scripts/app.js',
		watch: './src/scripts/**/*.js',
		output: './build/js'
	}
};

/* Definicion de tareas */

// Tarea para correr un web server de prueba
gulp.task('server', function() {
	// indicando de cual directorio va a leer, pipe es para indicar que funcion vamos a aplicarle
	gulp.src('./build')
		.pipe(webserver({
			host: '0.0.0.0',
			port: '8080',
			livereload: true
		}));
});

// Tarea para compilar stylus
gulp.task('build:css', function() {
	gulp.src(config.styles.main)
		.pipe(stylus({
			use: nib(),
			'include css': true //con esto le indicamos que tambien compile archivos que sean .css aparte de los .styl
		}))
		.pipe(minifyCSS())
		.pipe(gulp.dest(config.styles.output)); //indicamos a donde pone el css minificado y compilado
});

// Tarea para mover mi html de src a build
gulp.task('build:html', function() {
	gulp.src(config.html.main)
		.pipe(gulp.dest(config.html.output));
});

// Tarea para hacer watch de los cambios
gulp.task('watch', function() {
	gulp.watch(config.styles.watch, ['build:css']); //cuando cambie algo en nuestra carpeta, ejecuta la tarea
	gulp.watch(config.html.watch, ['build']);
	gulp.watch(config.scripts.watch, ['build:js']);
});

// Tarea para browserify, minificando el JS
gulp.task('build:js', function(){
	return browserify({ entries: config.scripts.main, debug: true, transform: [babelify.configure({'presets': [reactPreset, es2015Preset]})]})
		//.transform(babelify)
		.bundle()
		.pipe(source('bundle.js')) //con esto tomamos el archivo resultante de browserify
		.pipe(buffer()) //con esto pasamos ese archivo resultante a memoria para seguirlo trabajando
		.pipe(uglify()) //con esto lo minificamos
		.pipe(gulp.dest(config.scripts.output)); //por ultimo dejamos el archivo en el destino
});

/* Accesos a mis tareas */

gulp.task('build', ['build:css', 'build:html', 'build:js'])
gulp.task('default', ['server', 'watch', 'build'])

